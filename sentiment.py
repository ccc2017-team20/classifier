"""
Preprocess Raw tweets
Author:ccc2017-team20
Boya Chen, Ke Shi, Hanning Su, Lilian Tjong, Tjioe Christian
"""

import couchdb
import re
import string
import time
import numpy
import json
import sys
import matplotlib.path as mplPath

from textblob import TextBlob


"""
Get input from user or args
"""
if len(sys.argv) == 0 or len(sys.argv) < 7:
    SABoundaryJson = raw_input("SA boundary file (*.json list of geometry): ")
    LGABoundaryJson = raw_input("LGA boundary file (*.json list of geometry): ")
    keywordsJson = raw_input("Keyword list (*.json): ")
    couchdbServer = raw_input("CouchDB Server: ")
    sourcedbName = raw_input("RAW DB Name Source: ")
    targetdbName = raw_input("Target DB Name Source: ")
else:
    SABoundaryJson = sys.argv[1]
    LGABoundaryJson = sys.argv[2]
    keywordsJson = sys.argv[3]
    couchdbServer = sys.argv[4]
    sourcedbName = sys.argv[5]
    targetdbName = sys.argv[6]

# print "--------"
# print SABoundaryJson
# print LGABoundaryJson
# print keywordsJson
# print couchdbServer
# print sourcedbName
# print targetdbName
# print "--------"


"""
Directly load the json data as global variable
"""
with open(SABoundaryJson) as file:
    sa_boundary_coordinates = json.load(file)

with open(LGABoundaryJson) as file:
    lga_boundary_coordinates = json.load(file)

with open(keywordsJson) as file:
    keyword = json.load(file)
    accident_seeds = keyword['accident']
    party_seeds = keyword['party']
    transportation_seeds = keyword['transportation']
    tourism_seeds = keyword['tourism']

"""
Preprocess a text by removing non-alphanumeric characters
"""
def preprocess(text):
    printable = set(string.printable)
    text = filter(lambda x: x in printable, text)
    return (' '.join(re.sub("(@[A-Za-z0-9]+)|([^0-9A-Za-z \t])|(\w+:\/\/\S+)"," ",text).split())).lower()

"""
Get the content in <a> tag
"""
def get_tag_content(tag):
    regex = re.compile('<a.*?>')
    text = regex.sub('', tag)
    regex = re.compile('</a>')
    text = regex.sub('', text)
    return text

"""
Group time based on time range
"""
def convert_time(text):
    t = text.split(' ')[3].split(':')
    hour = int(t[0])

    if hour >= 0 and hour < 5:
        return '12 am - 5 am'
    elif hour >= 5 and hour < 10:
        return '5 am - 10 am'
    elif hour >= 10 and hour < 15:
        return '10 am - 3 pm'
    elif hour >= 15 and hour < 20:
        return '3 pm - 8 pm'
    return '8 pm - 12 am'


def get_sa_code(coordinate):

    for item in sa_boundary_coordinates:
        if item.has_key('geometry') and item['geometry'] is not None:
            if item['geometry']['type'] == 'Polygon':
                polygon = item['geometry']['coordinates'][0]
                path = mplPath.Path(numpy.array(polygon))
                point = coordinate[0], coordinate[1]
                if path.contains_point(point):
                    return item['properties']['SA2_MAIN11']
            else:
                polygons = item['geometry']['coordinates']
                for polygon in polygons:
                    path = mplPath.Path(numpy.array(polygon))
                    point = coordinate[0], coordinate[1]
                    if path.contains_point(point):
                        return item['properties']['SA2_MAIN11']

    return "-1"

def get_lga_code(coordinate):

    for item in lga_boundary_coordinates:
        if item.has_key('geometry') and item['geometry'] is not None:
            if item['geometry']['type'] == 'Polygon':
                polygon = item['geometry']['coordinates'][0]
                path = mplPath.Path(numpy.array(polygon))
                point = coordinate[0], coordinate[1]
                if path.contains_point(point):
                    return item['properties']['LGA_CODE11']
            else:
                polygons = item['geometry']['coordinates']
                for polygon in polygons:
                    path = mplPath.Path(numpy.array(polygon))
                    point = coordinate[0], coordinate[1]
                    if path.contains_point(point):
                        return item['properties']['LGA_CODE11']

    return "-1"

def get_topic(text):

    topics = []
    for t in text.split():
        
        if t in accident_seeds:
            topics.append('accident')
        if t in party_seeds:
            topics.append('party')
        if t in transportation_seeds:
            topics.append('transportation')
        if t in tourism_seeds:
            topics.append('tourism')
    topics = list(set(topics))

    if len(topics) == 0:
        topics.append('others')
    return topics

"""
Classify a tweet based on sentiment polarity
"""
def classify_data(sourcedb, targetdb):
    
    view = 'view_vic'
    index = 'new-view'
    dataset = sourcedb.view('_design/' + view + '/_view/' + index)

    # update_enabled = False
    # count = 0

    for row in dataset:
        docid = row['id']

        # if docid has been stored on targetdb, skip
        # unless update is enabled
        if targetdb.get(docid) is not None:
            continue
            """ Enabled this if you want to update the existing data with the new one """
            # if not update_enabled:
                # continue
            # before = targetdb[docid]
            # targetdb.delete(before)


        tweet = sourcedb[docid]

        # tweet data
        c_tweet = dict()
        c_tweet['_id'] = docid
        c_tweet['lang'] = tweet['lang']
        c_tweet['created_at'] = convert_time(tweet['created_at'])
        c_tweet['source'] = get_tag_content(tweet['source'])
        c_tweet['geometry'] = tweet['coordinates']
        

        temp_sa = get_sa_code(tweet['coordinates']['coordinates'])
        c_tweet['sa_code'] = int(temp_sa)


        temp_lga = get_lga_code(tweet['coordinates']['coordinates'])
        c_tweet['lga_code'] = int(temp_lga)


        # sentiment classification
        text = preprocess(tweet['text'])
        c_tweet['text'] = text
        c_tweet['topic'] = get_topic(text)
        c_tweet['sentiment'] = "positive" if TextBlob(text).sentiment[0] >= 0 else "negative"

        print c_tweet

        # insert into local database
        targetdb.save(c_tweet)

        # count += 1
        
        # if count == 10:
        #     break


if __name__ == "__main__":

    start = time.time()

    server = couchdb.Server(couchdbServer)
    sourcedb = server[sourcedbName]
    targetdb = server[targetdbName]

    classify_data(sourcedb, targetdb)

    end = time.time()
    print 'execution time:', str(end - start)